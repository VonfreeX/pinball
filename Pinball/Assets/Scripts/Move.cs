﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour {

    Vector3 FinalPos;
    public Vector3 InitialPos;
    public bool passed = false;
	// Use this for initialization
	void Start () {
        InitialPos = this.transform.position;
        FinalPos=new Vector3(1.23f,0.1f,-4.55f);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    private void OnTriggerExit(Collider other)
    {
        if(other.gameObject.tag=="ball")
        {
            if (!passed)
            {
                this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z + 0.7f);
                passed = true;
            }
        }
    }
}
