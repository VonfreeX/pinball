﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RightFlipper : MonoBehaviour {

    public float restPosition = 0F;
    public float pressedPosition = 45F;
    public float flipperStrength = 3000000F;
    public float flipperDamper = 25F;

    HingeJoint hinge;
    JointLimits limits;
    void Start()
    {

        hinge = GetComponent<HingeJoint>();
        limits = hinge.limits;

        this.GetComponent<HingeJoint>().useSpring = true;
    }

    // Update is called once per frame
    void Update()
    {
        JointSpring spring = new JointSpring();

        spring.spring = flipperStrength;
        spring.damper = flipperDamper;

        if (Input.GetKey(KeyCode.RightArrow))
        {
            spring.targetPosition = pressedPosition;
            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                this.GetComponent<AudioSource>().Play();
            }
        }
        else
            spring.targetPosition = restPosition;

        hinge.spring = spring;
        hinge.useLimits = true;
        limits.min = restPosition;
        limits.max = pressedPosition;
        hinge.limits = limits;

    }
}

