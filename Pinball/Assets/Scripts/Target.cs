﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Target : MonoBehaviour {
    bool Hit = false;
    static int cont;
   public bool moved = false;
    float timer = 0;
    int Score;
    Text score;
	// Use this for initialization
	void Start () {
        cont = 0;
        score = GameObject.Find("Plane/Canvas/ScoreNumber").GetComponent<Text>();
    }
	
	// Update is called once per frame
	void Update () {
        

	}
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "ball")
        {
            this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y-1, this.transform.position.z);
            Score = int.Parse(score.text) + 30;
            score.text = ("" + Score);
            moved = true;
            this.GetComponentInChildren<Light>().enabled = false;

           
            
        }
    }
}
