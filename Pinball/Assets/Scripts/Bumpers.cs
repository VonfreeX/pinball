﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bumpers : MonoBehaviour {
    public float bumperForce = 0.001f;
    public GameObject Light;
    bool ActiveLight = false;
    float Timer=0;
    public Text score;
     int Score;
    private AudioSource audio;
    // Use this for initialization
    void Start () {
        Light.SetActive(false);
        audio = this.GetComponent<AudioSource>();
        score = GameObject.Find("Plane/Canvas/ScoreNumber").GetComponent<Text>();
    }
	
	// Update is called once per frame
	void Update () {
		if (ActiveLight==true)
        {
            Timer = Timer + Time.deltaTime;
            if(Timer >=0.3f)
            {
                Timer = 0;
                ActiveLight = false;
                Light.SetActive(false);
            }
        }
	}
    

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "ball")
        {
            foreach (ContactPoint contact in collision.contacts)
            {
                contact.otherCollider.GetComponent<Rigidbody>().AddForce(-0.5f * contact.normal * bumperForce, ForceMode.Impulse);
            }
            Light.SetActive(true);
            ActiveLight = true;
           
            audio.Play();
            Score=int.Parse(score.text)+10;
            score.text = ("" + Score);
        }
    }
    
}
