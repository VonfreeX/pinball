﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TargetManager : MonoBehaviour
{
    public GameObject target1;
    public GameObject target2;
    public GameObject target3;
    public GameObject Launcher;
    public GameObject Ball;
    GameObject coin;
    int Score;
    Text score;
    float cont;
    Text Balls;
    bool moved = false;
    private AudioSource audio;
    // Use this for initialization
    void Start()
    {
        
        coin = GameObject.Find("insertcoin");
        target1 = GameObject.Find("Plane/Target1");
        target2 = GameObject.Find("Plane/Door/Target2");
        target3 = GameObject.Find("Plane/Target3");
        score = GameObject.Find("Plane/Canvas/ScoreNumber").GetComponent<Text>();
        Balls = GameObject.Find("Plane/Canvas/BallNumber").GetComponent<Text>();
        audio = this.GetComponent<AudioSource>();

    }

    // Update is called once per frame
    void Update()
    {
        if (coin.GetComponent<click>().clicked)
        {

            Ball.SetActive(true);
            Launcher.SetActive(true);
            coin.GetComponent<click>().clicked = false;
            Balls.text = ("" + 3);
            score.text = ("" + 0);
            audio.Play();

        }
        if (int.Parse(Balls.text)==0)
        {

            Ball.SetActive(false);
            Launcher.SetActive(false);
            audio.Stop();
            
        }
        if (target1.GetComponent<Target>().moved && target2.GetComponent<Target>().moved && target3.GetComponent<Target>().moved)
        {

            target1.GetComponent<Target>().moved = false;
            target2.GetComponent<Target>().moved = false;
            target3.GetComponent<Target>().moved = false;
            moved = true;
            Score = int.Parse(score.text) + 60;
            score.text = ("" + Score);
        }
        if (moved == true)
        {
            cont = cont + Time.deltaTime;
            if (cont > 1.5)
            {
                target1.GetComponentInChildren<Light>().enabled = true;
                target2.GetComponentInChildren<Light>().enabled = true;
                target3.GetComponentInChildren<Light>().enabled = true;
                target1.transform.position = new Vector3(target1.transform.position.x, target1.transform.position.y + 1, target1.transform.position.z);
                target2.transform.position = new Vector3(target2.transform.position.x, target2.transform.position.y + 1, target2.transform.position.z);
                target3.transform.position = new Vector3(target3.transform.position.x, target3.transform.position.y + 1, target3.transform.position.z);
                moved = false;
                cont = 0;
            }
        }
    }
}
