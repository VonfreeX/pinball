﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PutBall : MonoBehaviour {
    Text Balls;
    int numBalls;
    public GameObject box;
    // Use this for initialization
    void Start () {
        Balls = GameObject.Find("Plane/Canvas/BallNumber").GetComponent<Text>();
        
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "ball")
        {
            numBalls = int.Parse(Balls.text)-1;
            Balls.text = ("" + numBalls);
            collision.transform.position = new Vector3(5.64f,2.26f, -6.58f);
            box.transform.position = box.GetComponent<Move>().InitialPos;
            box.GetComponent<Move>().passed = false;

        }

    }
}
