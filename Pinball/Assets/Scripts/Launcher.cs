﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Launcher : MonoBehaviour
{
    public Rigidbody rb;
    public float force;
    bool launched = true;
    Vector3 pos_ini;
    GameObject ball;
    float timePressed;
    // Use this for initialization
    void Start()
    {
        ball = GameObject.Find("Ball");
       
        pos_ini = this.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            if (timePressed <= 2)
            {
                this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z-0.0009f);
                timePressed = timePressed + Time.deltaTime;
            }
        }
        if (Input.GetKeyUp(KeyCode.Space) && launched == false)
        {
            ball.GetComponent<Rigidbody>().AddForce(0, 0, force * timePressed, ForceMode.Impulse);
            launched = true;
            timePressed = 0;
          
        }
        if (Input.GetKeyUp(KeyCode.Space) )
        {
            this.transform.position = pos_ini;
        }

   }
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "ball")
        {
            launched = false;

        }

    }
}
