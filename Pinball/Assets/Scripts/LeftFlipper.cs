﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeftFlipper : MonoBehaviour
{

   public float InitPos = 0F;
   public float FinalPos = -45F;
    public float Hit = 3000000F;
   public float flipperDamper = 25F;

    HingeJoint hinge;
    JointLimits limits;
    void Start()
    {

        hinge= GetComponent<HingeJoint>();
        limits = hinge.limits;
        
        this.GetComponent<HingeJoint>().useSpring = true;
    }

    // Update is called once per frame
    void Update()
    {
        JointSpring spring = new JointSpring();

        spring.spring = Hit;
        spring.damper = flipperDamper;

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            spring.targetPosition = FinalPos;
            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                this.GetComponent<AudioSource>().Play();
            }

        }
        
        else
            spring.targetPosition = InitPos;

        hinge.spring = spring;
        hinge.useLimits = true;
        limits.min = InitPos;
        limits.max = FinalPos;
        hinge.limits = limits;

    }
}
